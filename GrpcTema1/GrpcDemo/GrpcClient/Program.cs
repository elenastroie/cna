﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcServer;

namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.Write("Nume: ");
            var name = Console.ReadLine();
            var input = new HelloRequest { Name = name };

            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);
           
            await client.SayHelloAsync(input);

            Console.ReadLine();
        }
    }
}
