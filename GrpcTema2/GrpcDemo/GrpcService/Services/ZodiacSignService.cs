﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;

namespace GrpcService.Services
{
    public class ZodiacSignService : ZodiacSign.ZodiacSignBase
    {
        public ZodiacSignService()
        {

        }

        public override Task<ZodiacSignReply> GetZodiacSign(ZodiacSignRequest request, ServerCallContext context)
        {
            ZodiacSignReply reply = new ZodiacSignReply();
            var birthdayDetails = request.Birthday.Split("/");
            foreach (var period in Startup.zodiacSignsPeriods)
            {
                var periodDetails = period.Split(" ");
                var firstMonth = periodDetails[0].Split("/").First();
                var secondMonth = periodDetails[1].Split("/").First();

                if (firstMonth == birthdayDetails[0] || firstMonth == "0" + birthdayDetails[0])
                {
                    var day = Int64.Parse(periodDetails[0].Split("/").Last());

                    if (Int16.Parse(birthdayDetails[1]) > day)
                    {
                        reply.Sign = periodDetails[2];
                    }
                }
                else if(secondMonth == birthdayDetails[0] || secondMonth == "0" + birthdayDetails[0])
                {
                    var day = Int64.Parse(periodDetails[1].Split("/").Last());

                    if (Int16.Parse(birthdayDetails[1]) < day)
                    {
                        reply.Sign = periodDetails[2];
                    }
                }
            }
            return Task.FromResult(reply);
        }
    }
}
