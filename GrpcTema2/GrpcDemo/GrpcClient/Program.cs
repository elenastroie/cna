﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcService;

namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string date;
            date = Console.ReadLine();
            while (!IsDateValid(date))
            {
                Console.WriteLine("CORESPUNZATOR");
                date = Console.ReadLine();
            }
            var birthday = new ZodiacSignRequest{Birthday = date};
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new ZodiacSign.ZodiacSignClient(channel);
            var reply = await client.GetZodiacSignAsync(birthday);
            Console.WriteLine(reply.Sign);
            Console.ReadLine();
        }

        private static bool IsDateValid(string date)
        {
            Regex dateValidator = new Regex(@"^(0?[1-9]|1[0-2])/(0?[1-9]|1\d|2\d|3[01])/[1-9]\d{3}$");
            //var dateValidator = @"^(((0?[1-9]|1[0-2])/(0?[1-9]|1\d|2\d|3[01])/[1-9]d{3}$";
            if (dateValidator.IsMatch(date))
            {
                if (date.StartsWith("2")|| date.StartsWith("02"))
                {
                    var startPosition = date.IndexOf("/") + 1;
                    var day = Int16.Parse(date.Substring(startPosition,
                        date.IndexOf("/", startPosition) - startPosition));

                    if (day < 29)
                    {
                        return true;
                    }
                    else
                    {
                        if (day > 29)
                        {
                            return false;
                        }
                        else
                        {
                            var year = Int64.Parse(date.Split("/").Last());

                            if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
                                return true;
                            else
                                return false;
                        }
                    }
                }

                return true;
            }

            return false;
        }
        
    }
}